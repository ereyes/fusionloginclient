const express = require('express');
const passport = require('passport');
const OAuth2Strategy = require('passport-oauth2').Strategy
const jwt = require('jsonwebtoken')

require('dotenv').config();

const url = process.env.URL
const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRETS = process.env.CLIENT_SECRETS
const FUSION_URL = process.env.FUSION_URL

const app = express();
let token = ''

app.set('view engine', 'ejs')

app.use(passport.initialize());
app.use(passport.session());

app.get('/', (req, res) => res.render('home', { root: __dirname }));
app.get('/logout', (req, res) => res.render('logout', { root: __dirname }));
app.get('/success', (req, res) => res.send(res.msg + ". You have successfully logged in "));
app.get('/error', (req, res) => res.send("error logging in"));
app.get('/exit', function (req, res) {
    req.logout();
    res.redirect(`${FUSION_URL}/oauth2/logout?client_id=${CLIENT_ID}`);
});

passport.serializeUser(function (user, cb) {
    cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
    cb(null, obj);
});

passport.use(new OAuth2Strategy({
    authorizationURL: `${FUSION_URL}/oauth2/authorize`,
    tokenURL: `${FUSION_URL}/oauth2/token`,
    clientID: CLIENT_ID,
    clientSecret: CLIENT_SECRETS,
    callbackURL: `${url}/auth/oauth2/callback`,
    response_type: 'token',
    scope: ['profile']
},
    function (accessToken, refreshToken, profile, res, cb) {
        payload = jwt.decode(accessToken)
        token = accessToken
        //console.log('profile:')
        //console.log(profile)
        return cb(null, payload)
    }
));

//http://127.0.0.1:5000/auth/fusion/callback

app.get('/auth/oauth2',
    passport.authenticate('oauth2'));


app.get('/auth/oauth2/callback',
    passport.authenticate('oauth2', { failureRedirect: '/' }),
    function (req, res) {
        //console.log('auth/fusion/callback: ')
        //console.log(token)
        //Successful authentication, redirect home.
        
        res.render('secret', { user: req.user, token: token, query: req.query});

    });



const port = process.env.PORT ||  5000;
app.listen(port, () => console.log('App listening on port ' + port));
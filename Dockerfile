FROM node:8.15.0-alpine

# Create app directory
RUN mkdir -p /app
WORKDIR /app

# Bundle app source
COPY . /app
RUN npm install --verbose

CMD [ "node", "src/index.js" ]

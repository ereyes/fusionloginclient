# fusionloginclient
Simple web app to test fusionauth login

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development purposes. See deployment for notes on how to deploy the project on a live system.

````
git clone https://gitlab.com/ereyes/fusionloginclient.git

````

### Prerequisites

Docker and docker-compose are required for use

#### Docker

for windows https://docs.docker.com/docker-for-windows/install/
for ubuntu https://docs.docker.com/install/linux/docker-ce/ubuntu/
for centos https://docs.docker.com/install/linux/docker-ce/centos/
for debian https://docs.docker.com/install/linux/docker-ce/debian/
for fedora https://docs.docker.com/install/linux/docker-ce/fedora/

#### docker-compose

for all platform https://docs.docker.com/compose/install/

### Installing

After having docker and docker-compose ready you only have to execute a command console in the folder where you clone the project and execute the following command

```
docker-compose up
```

See  ```docker-compose --help``` to see other available commands. You can also install command completion for the bash and zsh shell, which also shows you available commands.

## .env content
```
CLIENT_ID=fusion_app_client_id
CLIENT_SECRETS=fusion_app_client_secret
FUSION_URL=fusion_service_url
PORT=port number to deplot de fusionclient

```

## Running de app
Run the app with:

```
docker-compose up
```

The application running launch this messages:

```
App listening on port 5000
```

Open this URL in the browser and the following link will be shown:


[Sing with fusion](https:/fusionauth_url/)

This link will redirect to fusion aunthentication form to authorize the user to use the app. If user introduced is authorized fusionauth will send back o the app with de user with authorized status. The user profile object is printed on console.



## Built With

* [NodeJS](https://nodejs.org/es/) - The web framework used
* [npm](https://www.npmjs.com) - Dependency Management

## Contributing

https://gitlab.com/bismaymp


## Versioning

version 1.0.0

## Authors

https://gitlab.com/bismaymp

## License

MIT